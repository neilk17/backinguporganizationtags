# README #

Lambda function used to store all user ID's and Tags into a .csv file stored in an s3 bucket.


### What is this repository for? ###

Once lambda is deployed, it will collect organization accounts and their tags and store them into an s3 bucket 
* Version 1.0

### How do I get set up? ###

[ Tutorial to deploy the lambda ](https://docs.aws.amazon.com/lambda/latest/dg/lambda-python-how-to-create-deployment-package.html)

