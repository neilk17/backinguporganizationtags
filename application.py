''' 
Lambda to write Organization tags into an s3 bucket
'''

import boto3
import os
import logging
import csv
from datetime import datetime
import s3fs


# Creating session 
session = boto3.session.Session(region_name=os.environ['region_name'])
boto_sts=session.client('sts')
s3_client = session.client('s3')


def get_accounts(client, accum_accounts = [], token = None, max_results = 10):
    """
    Listing out all organizations accounts ids
    """
    if token:
        response = client.list_accounts(NextToken = token, MaxResults = max_results)
    else:
        response = client.list_accounts(MaxResults = max_results)

    accounts = response['Accounts']
    logging.info("Accounts fetched: {}".format(len(accounts)))

    token = response.get('NextToken',None)
    
    current_accounts = []
    for account in accounts:
        current_accounts.append(account['Id'])

    if token:
        return get_accounts(client, accum_accounts + current_accounts, 
                            token = token,
                            max_results = max_results)
        
    return  accum_accounts + current_accounts

def write_to_csv_file(data, filename, count):
    """
    Write to a csv file.
    :param data:
    :param filename:
    :return:
    """
    if data is None:
        return
    
    s3 = s3fs.S3FileSystem(anon=False)
    filepath = "{0}/{1}".format(os.environ['bucket_name'], filename)
    with s3.open(filepath, 'a', newline='\n') as csvfile:
        writer = csv.DictWriter(csvfile, delimiter=',', fieldnames=data.keys(),
                                quotechar='"')
        if count < 1 :
            writer.writeheader()
        writer.writerow(data)


def main(context, event):
    RoleList = os.environ['cross_account_role_arn'].split(',')
    file_name = 'org_tags_{}.csv'.format(datetime.now())
    count = 0
    for Role in RoleList:
        stsresponse = boto_sts.assume_role(RoleArn=Role, RoleSessionName='newsession')
        
        logging.info(stsresponse)
        # Save the details from assumed role into vars
        newsession_id = stsresponse["Credentials"]["AccessKeyId"]
        newsession_key = stsresponse["Credentials"]["SecretAccessKey"]
        newsession_token = stsresponse["Credentials"]["SessionToken"]
        
        session_master_account = session.client('organizations', 
                                    region_name=os.environ['region_name'],
                                    aws_access_key_id=newsession_id,
                                    aws_secret_access_key=newsession_key, 
                                    aws_session_token=newsession_token)
        
        accounts = sorted(get_accounts(session_master_account))
        total_accounts = len(accounts)
        
        

        
        for account in accounts:
            tag = session_master_account.list_tags_for_resource(ResourceId=account)
            # writer.writerow()
            dict1 = {'account': account, 'tags': tag['Tags']}
            write_to_csv_file(dict1, file_name, count)
            count += 1
            

if __name__ == '__main__':
    pass